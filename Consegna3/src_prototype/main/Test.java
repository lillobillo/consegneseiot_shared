package main;

import java.io.IOException;

import common.BasicEventLoopController;
import common.Event;
import devices.ButtonPressed;
import devices.Light;
import devices_impl.Button;
import devices_impl.Led;

public class Test extends BasicEventLoopController {

    Button but;
    Light led;

    public Test() {
        but = new Button(1);
        led = new Led(6);
        but.addObserver(this);
    }

    @Override
    protected void processEvent(Event ev) {
        System.out.println("pressed");
        // TODO Auto-generated method stub
        if (ev instanceof ButtonPressed) {
            try {
                led.switchOn();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } else {
            try {
                led.switchOff();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

}
